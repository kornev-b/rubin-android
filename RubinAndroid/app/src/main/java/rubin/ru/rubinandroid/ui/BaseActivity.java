package rubin.ru.rubinandroid.ui;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import rubin.ru.rubinandroid.Config;
import rubin.ru.rubinandroid.R;
import rubin.ru.rubinandroid.model.User;
import rubin.ru.rubinandroid.network.SessionRestManager;
import rubin.ru.rubinandroid.ui.view.ScrimInsetsScrollView;
import rubin.ru.rubinandroid.util.LUtils;
import rubin.ru.rubinandroid.util.LogUtils;
import rubin.ru.rubinandroid.util.PrefUtils;
import rubin.ru.rubinandroid.util.RecentTasksStyler;
import rubin.ru.rubinandroid.util.UIUtils;

/**
 * Created by Bogdan Kornev
 * on 09.02.2015 at 16:48.
 */
public class BaseActivity extends ActionBarActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    protected final String TAG;
    private final static String ACTION_BAR_SHOWN_KEY = "actionBarShown";
    private ObjectAnimator statusBarColorAnimator;
    // When set, these components will be shown/hidden in sync with the action bar
    // to implement the "quick recall" effect (the Action Bar and the header views disappear
    // when you scroll down a list, and reappear quickly when you scroll up).
    private ArrayList<View> hideableHeaderViews = new ArrayList<View>();

    // Durations for certain animations we use:
    private static final int HEADER_HIDE_ANIM_DURATION = 300;
    private static final int ACCOUNT_BOX_EXPAND_ANIM_DURATION = 200;

    // symbols for navdrawer items (indices must correspond to array below). This is
    // not a list of items that are necessarily *present* in the Nav Drawer; rather,
    // it's a list of all possible items.
    protected static final int NAVDRAWER_ITEM_NEWS = 0;
    protected static final int NAVDRAWER_ITEM_RUBIN_TV = 1;
    protected static final int NAVDRAWER_ITEM_SEASON = 2;
    protected static final int NAVDRAWER_ITEM_STANDINGS = 3;
    protected static final int NAVDRAWER_ITEM_CLUB = 4;
    protected static final int NAVDRAWER_ITEM_TEAM = 5;
    protected static final int NAVDRAWER_ITEM_PLACES = 6;
    protected static final int NAVDRAWER_ITEM_STORE = 7;
    protected static final int NAVDRAWER_ITEM_TICKETS = 8;
    protected static final int NAVDRAWER_ITEM_GUEST_BOOK = 9;
    private static final int NAVDRAWER_ITEM_SETTINGS = 10;
    protected static final int NAVDRAWER_ITEM_INVALID = -1;
    protected static final int NAVDRAWER_ITEM_SEPARATOR = -2;

    // titles for navdrawer items (indices must correspond to the above)
    private static final int[] NAVDRAWER_TITLE_RES_ID = new int[]{
            R.string.navdrawer_title_news,
            R.string.navdrawer_title_rubin_tv,
            R.string.navdrawer_title_season,
            R.string.navdrawer_title_standings,
            R.string.navdrawer_title_club,
            R.string.navdrawer_title_team,
            R.string.navdrawer_title_places,
            R.string.navdrawer_title_store,
            R.string.navdrawer_title_tickets,
            R.string.navdrawer_title_guests_book,
            R.string.navdrawer_title_settings
    };

    // icons for navdrawer items (indices must correspond to above array)
    private static final int[] NAVDRAWER_ICON_RES_ID = new int[]{
            R.drawable.ic_assignment_black_24dp,
            R.drawable.ic_tv_black_24dp,
            R.drawable.ic_assignment_black_24dp,
            R.drawable.ic_assignment_black_24dp,
            R.drawable.ic_assignment_black_24dp,
            R.drawable.ic_assignment_black_24dp,
            R.drawable.ic_map_black_24dp,
            R.drawable.ic_store_black_24dp,
            R.drawable.ic_assignment_black_24dp,
            R.drawable.ic_question_answer_black_24dp,
            R.drawable.ic_settings_black_24dp
    };

    // delay to launch nav drawer item, to allow close animation to play
    private static final int NAVDRAWER_LAUNCH_DELAY = 250;

    // fade in and fade out durations for the main content when switching between
    // different Activities of the app through the Nav Drawer
    private static final int MAIN_CONTENT_FADEOUT_DURATION = 150;
    private static final int MAIN_CONTENT_FADEIN_DURATION = 250;

    // list of navdrawer items that were actually added to the navdrawer, in order
    private ArrayList<Integer> navDrawerItems = new ArrayList<>();

    // views that correspond to each navdrawer item, null if not yet created
    private View[] navDrawerItemViews = null;
    // variables that control the Action Bar auto hide behavior (aka "quick recall")
    private boolean actionBarAutoHideEnabled = false;
    private int actionBarAutoHideSensivity = 0;
    private int actionBarAutoHideMinY = 0;
    private int actionBarAutoHideSignal = 0;

    private LUtils lUtils;

    // A Runnable that we should execute when the navigation drawer finishes its closing animation
    private Runnable deferredOnDrawerClosedRunnable;
    private Handler handler;

    private int mThemedStatusBarColor;
    private int mNormalStatusBarColor;
    private static final TypeEvaluator ARGB_EVALUATOR = new ArgbEvaluator();

    // views
    private DrawerLayout drawerLayout;
    private ViewGroup drawerItemsListContainer;
    private Toolbar actionBarToolbar;

    // retained state
    protected boolean actionBarShown = true;

    {
        TAG = getClass().getSimpleName();
    }

    private SwipeRefreshLayout swipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RecentTasksStyler.styleRecentTasksEntry(this);

        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.registerOnSharedPreferenceChangeListener(this);

        handler = new Handler();

        mThemedStatusBarColor = getResources().getColor(R.color.theme_primary_dark);
        mNormalStatusBarColor = mThemedStatusBarColor;

        if (null != savedInstanceState) {
            actionBarShown = savedInstanceState.getBoolean(ACTION_BAR_SHOWN_KEY);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(ACTION_BAR_SHOWN_KEY, actionBarShown);
    }

    /**
     * Returns the navigation drawer item that corresponds to this Activity. Subclasses
     * of BaseActivity override this to indicate what nav drawer item corresponds to them
     * Return NAVDRAWER_ITEM_INVALID to mean that this Activity should not have a Nav Drawer.
     */
    protected int getSelfNavDrawerItem() {
        return NAVDRAWER_ITEM_INVALID;
    }

    /**
     * Sets up the navigation drawer as appropriate. Note that the nav drawer will be
     * different depending on whether the attendee indicated that they are attending the
     * event on-site vs. attending remotely.
     */
    private void setupNavDrawer() {
        // What nav drawer item should be selected?
        int selfItem = getSelfNavDrawerItem();

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawerLayout == null) {
            return;
        }
        drawerLayout.setStatusBarBackgroundColor(
                getResources().getColor(R.color.theme_primary_dark));
        ScrimInsetsScrollView navDrawer = (ScrimInsetsScrollView)
                drawerLayout.findViewById(R.id.navdrawer);
        if (selfItem == NAVDRAWER_ITEM_INVALID) {
            // do not show a nav drawer
            if (navDrawer != null) {
                ((ViewGroup) navDrawer.getParent()).removeView(navDrawer);
            }
            drawerLayout = null;
            return;
        }

        if (navDrawer != null) {
            final View chosenAccountContentView = findViewById(R.id.account_content_view);
            final View chosenAccountView = findViewById(R.id.account_view);
            final int navDrawerChosenAccountHeight = getResources().getDimensionPixelSize(
                    R.dimen.navdrawer_account_height);
            navDrawer.setOnInsetsCallback(new ScrimInsetsScrollView.OnInsetsCallback() {
                @Override
                public void onInsetsChanged(Rect insets) {
                    ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams)
                            chosenAccountContentView.getLayoutParams();
                    lp.topMargin = insets.top;
                    chosenAccountContentView.setLayoutParams(lp);

                    ViewGroup.LayoutParams lp2 = chosenAccountView.getLayoutParams();
                    lp2.height = navDrawerChosenAccountHeight + insets.top;
                    chosenAccountView.setLayoutParams(lp2);
                }
            });
        }

        if (actionBarToolbar != null) {
            actionBarToolbar.setNavigationIcon(R.drawable.ic_drawer);
            actionBarToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    drawerLayout.openDrawer(Gravity.START);
                }
            });
        }

        drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerClosed(View drawerView) {
                // run deferred action, if we have one
                if (deferredOnDrawerClosedRunnable != null) {
                    deferredOnDrawerClosedRunnable.run();
                    deferredOnDrawerClosedRunnable = null;
                }
                onNavDrawerStateChanged(false, false);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                onNavDrawerStateChanged(true, false);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                onNavDrawerStateChanged(isNavDrawerOpen(), newState != DrawerLayout.STATE_IDLE);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                onNavDrawerSlide(slideOffset);
            }
        });

        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);

        // populate the nav drawer with the correct items
        populateNavDrawer();

        // When the user runs the app for the first time, we want to land them with the
        // navigation drawer open. But just the first time.
        if (PrefUtils.isFirstLaunch(this)) {
            // first run of the app starts with the nav drawer open
            drawerLayout.openDrawer(Gravity.START);
        }
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        getActionBarToolbar();
    }

    // Subclasses can override this for custom behavior
    protected void onNavDrawerStateChanged(boolean isOpen, boolean isAnimating) {
        if (actionBarAutoHideEnabled && isOpen) {
            autoShowOrHideActionBar(true);
        }
    }

    protected void closeNavDrawer() {
        if (drawerLayout != null) {
            drawerLayout.closeDrawer(Gravity.START);
        }
    }

    protected void onNavDrawerSlide(float offset) {
    }

    protected boolean isNavDrawerOpen() {
        return drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.START);
    }

    /**
     * Populates the navigation drawer with the appropriate items.
     */
    private void populateNavDrawer() {
        navDrawerItems.clear();
        // decide which items will appear in the nav drawer
        navDrawerItems.add(NAVDRAWER_ITEM_NEWS);
        navDrawerItems.add(NAVDRAWER_ITEM_RUBIN_TV);
        navDrawerItems.add(NAVDRAWER_ITEM_SEASON);
        navDrawerItems.add(NAVDRAWER_ITEM_STANDINGS);
        navDrawerItems.add(NAVDRAWER_ITEM_CLUB);
        navDrawerItems.add(NAVDRAWER_ITEM_TEAM);
        navDrawerItems.add(NAVDRAWER_ITEM_PLACES);
        navDrawerItems.add(NAVDRAWER_ITEM_STORE);
        navDrawerItems.add(NAVDRAWER_ITEM_TICKETS);
        navDrawerItems.add(NAVDRAWER_ITEM_GUEST_BOOK);
        navDrawerItems.add(NAVDRAWER_ITEM_SETTINGS);
        createNavDrawerItems();
    }

    @Override
    public void onBackPressed() {
        if (isNavDrawerOpen()) {
            closeNavDrawer();
        } else {
            super.onBackPressed();
        }
    }

    private void createNavDrawerItems() {
        drawerItemsListContainer = (ViewGroup) findViewById(R.id.navdrawer_items_list);
        if (drawerItemsListContainer == null) {
            return;
        }

        navDrawerItemViews = new View[navDrawerItems.size()];
        drawerItemsListContainer.removeAllViews();
        int i = 0;
        for (int itemId : navDrawerItems) {
            navDrawerItemViews[i] = makeNavDrawerItem(itemId, drawerItemsListContainer);
            drawerItemsListContainer.addView(navDrawerItemViews[i]);
            ++i;
        }
    }

    /**
     * Sets up the given navdrawer item's appearance to the selected state. Note: this could
     * also be accomplished (perhaps more cleanly) with state-based layouts.
     */
    private void setSelectedNavDrawerItem(int itemId) {
        if (navDrawerItemViews != null) {
            for (int i = 0; i < navDrawerItemViews.length; i++) {
                if (i < navDrawerItems.size()) {
                    int thisItemId = navDrawerItems.get(i);
                    formatNavDrawerItem(navDrawerItemViews[i], thisItemId, itemId == thisItemId);
                }
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(PrefUtils.PREF_USER)) {
            LogUtils.LOGD(TAG, "User preference is changed.");
            populateNavDrawer();
            invalidateOptionsMenu();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setupNavDrawer();
        setupAccountBox();

        trySetupSwipeRefresh();

        View mainContent = findViewById(R.id.main_content);
        if (mainContent != null) {
            mainContent.setAlpha(0);
            mainContent.animate().alpha(1).setDuration(MAIN_CONTENT_FADEIN_DURATION);
        } else {
            LogUtils.LOGW(TAG, "No view with ID main_content to fade in.");
        }
    }

    private void trySetupSwipeRefresh() {
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refreshLayout);
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setColorSchemeResources(
                    R.color.refresh_progress_1,
                    R.color.refresh_progress_2,
                    R.color.refresh_progress_3);
        }
    }

    /**
     * Sets up the account box. The account box is the area at the top of the nav drawer that
     * shows which account the user is logged in as, and lets them switch accounts. It also
     * shows the user's cover photo as background.
     */
    private void setupAccountBox() {
        View accountHeader = findViewById(R.id.account_view);

        if (accountHeader == null) {
            //This activity does not have an account box
            return;
        }

        User user = PrefUtils.getUser(this);
        if (user == null) {
            return;
        }
        int avatarSize = getResources().getDimensionPixelSize(R.dimen.navdrawer_profile_image_size);
        Picasso.with(this)
                .load(String.format(Config.RUBIN_IMAGES_URL_PREFIX + user.getAvatar()))
                .resize(avatarSize, avatarSize)
                .into((ImageView) accountHeader.findViewById(R.id.profile_image));

//        int bgWidth = (int) getActivity().getResources().getDimension(R.dimen.navdrawer_width);
//        int bgHeight = (int) getActivity().getResources().getDimension(R.dimen.navdrawer_account_height);

        ((TextView) accountHeader.findViewById(R.id.profile_name_text)).setText(user.getLogin());
        ((TextView) accountHeader.findViewById(R.id.profile_email_text)).setText(user.getEmail());

//        accountHeader.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // todo show user activity
//                closeNavDrawer();
//            }
//        });
    }

    private void goToNavDrawerItem(int item) {
        Intent intent;
        switch (item) {
            case NAVDRAWER_ITEM_NEWS:
                intent = new Intent(this, NewsActivity.class);
                startActivity(intent);
                finish();
                break;
            // todo check what we done
        }
    }

    private void onNavDrawerItemClicked(final int itemId) {
        if (itemId == getSelfNavDrawerItem()) {
            drawerLayout.closeDrawer(Gravity.START);
            return;
        } else if(itemId != NAVDRAWER_ITEM_NEWS) {
            // todo check what we done
            Toast.makeText(this, "TODO", Toast.LENGTH_SHORT).show();
            return;
        }

        if (isSpecialItem(itemId)) {
            goToNavDrawerItem(itemId);
        } else {
            // launch the target Activity after a short delay, to allow the close animation to play
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    goToNavDrawerItem(itemId);
                }
            }, NAVDRAWER_LAUNCH_DELAY);

            // change the active item on the list so the user can see the item changed
            setSelectedNavDrawerItem(itemId);
            // fade out the main content
            View mainContent = findViewById(R.id.main_content);
            if (mainContent != null) {
                mainContent.animate().alpha(0).setDuration(MAIN_CONTENT_FADEOUT_DURATION);
            }
        }

        drawerLayout.closeDrawer(Gravity.START);
    }

    /**
     * Initializes the Action Bar auto-hide (aka Quick Recall) effect.
     */
    private void initActionBarAutoHide() {
        actionBarAutoHideEnabled = true;
        actionBarAutoHideMinY = getResources().getDimensionPixelSize(
                R.dimen.action_bar_auto_hide_min_y);
        actionBarAutoHideSensivity = getResources().getDimensionPixelSize(
                R.dimen.action_bar_auto_hide_sensivity);
    }

    /**
     * Indicates that the main content has scrolled (for the purposes of showing/hiding
     * the action bar for the "action bar auto hide" effect). currentY and deltaY may be exact
     * (if the underlying view supports it) or may be approximate indications:
     * deltaY may be INT_MAX to mean "scrolled forward indeterminately" and INT_MIN to mean
     * "scrolled backward indeterminately".  currentY may be 0 to mean "somewhere close to the
     * start of the list" and INT_MAX to mean "we don't know, but not at the start of the list"
     */
    public void onMainContentScrolled(int currentY, int deltaY) {
        if (deltaY > actionBarAutoHideSensivity) {
            deltaY = actionBarAutoHideSensivity;
        } else if (deltaY < -actionBarAutoHideSensivity) {
            deltaY = -actionBarAutoHideSensivity;
        }

        if (Math.signum(deltaY) * Math.signum(actionBarAutoHideSignal) < 0) {
            // deltaY is a motion opposite to the accumulated signal, so reset signal
            actionBarAutoHideSignal = deltaY;
        } else {
            // add to accumulated signal
            actionBarAutoHideSignal += deltaY;
        }

        boolean shouldShow = currentY < actionBarAutoHideMinY ||
                (actionBarAutoHideSignal <= -actionBarAutoHideSensivity);
        autoShowOrHideActionBar(shouldShow);
    }

    public Toolbar getActionBarToolbar() {
        if (actionBarToolbar == null) {
            actionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
            if (actionBarToolbar != null) {
                setSupportActionBar(actionBarToolbar);
            }
        }
        return actionBarToolbar;
    }

    protected void autoShowOrHideActionBar(boolean show) {
        if (show == actionBarShown) {
            return;
        }

        actionBarShown = show;
        onActionBarAutoShowOrHide(show);
    }

    protected void enableActionBarAutoHide() {
        initActionBarAutoHide();
        if (swipeRefreshLayout != null) {
            int abHeight = UIUtils.calculateActionBarSize(this);
            swipeRefreshLayout.setProgressViewOffset(false, abHeight, abHeight * 2);
        }
    }

    protected void enableActionBarAutoHide(final RecyclerView recyclerView) {
        initActionBarAutoHide();
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            final static int ITEMS_THRESHOLD = 3;
            int lastFvi = 0;

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int firstVisibleItem = getFirstVisibleItem(recyclerView);
                onMainContentScrolled(firstVisibleItem <= ITEMS_THRESHOLD ? 0 : Integer.MAX_VALUE,
                        lastFvi - firstVisibleItem > 0 ? Integer.MIN_VALUE :
                                lastFvi == firstVisibleItem ? 0 : Integer.MAX_VALUE
                );
                lastFvi = firstVisibleItem;
            }
        });
    }

    private int getFirstVisibleItem(RecyclerView recyclerView) {
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof LinearLayoutManager) {
            return ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
        }
        return ((StaggeredGridLayoutManager) layoutManager).findFirstVisibleItemPositions(null)[0];
    }

    private View makeNavDrawerItem(final int itemId, ViewGroup container) {
        boolean selected = getSelfNavDrawerItem() == itemId;
        int layoutToInflate;
        if (itemId == NAVDRAWER_ITEM_SEPARATOR) {
            layoutToInflate = R.layout.navdrawer_separator;
        } else {
            layoutToInflate = R.layout.navdrawer_item;
        }
        View view = getLayoutInflater().inflate(layoutToInflate, container, false);

        if (isSeparator(itemId)) {
            // we are done
            UIUtils.setAccessibilityIgnore(view);
            return view;
        }

        ImageView iconView = (ImageView) view.findViewById(R.id.icon);
        TextView titleView = (TextView) view.findViewById(R.id.title);
        int iconId = itemId >= 0 && itemId < NAVDRAWER_ICON_RES_ID.length ?
                NAVDRAWER_ICON_RES_ID[itemId] : 0;
        int titleId = itemId >= 0 && itemId < NAVDRAWER_TITLE_RES_ID.length ?
                NAVDRAWER_TITLE_RES_ID[itemId] : 0;

        // set icon and text
        iconView.setVisibility(iconId > 0 ? View.VISIBLE : View.GONE);
        if (iconId > 0) {
            iconView.setImageResource(iconId);
        }
        titleView.setText(getString(titleId));

        formatNavDrawerItem(view, itemId, selected);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNavDrawerItemClicked(itemId);
            }
        });

        return view;
    }

    private boolean isSpecialItem(int itemId) {
        return itemId == NAVDRAWER_ITEM_SETTINGS;
    }

    private boolean isSeparator(int itemId) {
        return itemId == NAVDRAWER_ITEM_SEPARATOR;
    }

    private void formatNavDrawerItem(View view, int itemId, boolean selected) {
        if (isSeparator(itemId)) {
            // not applicable
            return;
        }

        ImageView iconView = (ImageView) view.findViewById(R.id.icon);
        TextView titleView = (TextView) view.findViewById(R.id.title);

        if (selected) {
            view.setBackgroundResource(R.drawable.selected_navdrawer_item_background);
        }

        // configure its appearance according to whether or not it's selected
        titleView.setTextColor(selected ?
                getResources().getColor(R.color.navdrawer_text_color_selected) :
                getResources().getColor(R.color.navdrawer_text_color));
        iconView.setColorFilter(selected ?
                getResources().getColor(R.color.navdrawer_icon_tint_selected) :
                getResources().getColor(R.color.navdrawer_icon_tint));
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        PrefUtils.unregisterOnSharedPreferenceChangeListener(this, this);
    }


    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
        updateActionBarTitle();
    }

    @Override
    public void setTitle(int titleId) {
        super.setTitle(titleId);
        updateActionBarTitle();
    }

    private void updateActionBarTitle() {
        getSupportActionBar().setTitle(getTitle());
    }

    protected void registerHideableHeaderView(View hideableHeaderView) {
        if (!hideableHeaderViews.contains(hideableHeaderView)) {
            hideableHeaderViews.add(hideableHeaderView);
        }
    }

    protected void unregisterHideableHeaderView(View hideableHeaderView) {
        if (hideableHeaderViews.contains(hideableHeaderView)) {
            hideableHeaderViews.remove(hideableHeaderView);
        }
    }

    public LUtils getLUtils() {
        return lUtils;
    }

    public int getThemedStatusBarColor() {
        return mThemedStatusBarColor;
    }

    public void setNormalStatusBarColor(int color) {
        mNormalStatusBarColor = color;
        if (drawerLayout != null) {
            drawerLayout.setStatusBarBackgroundColor(mNormalStatusBarColor);
        }
    }

    protected void onActionBarAutoShowOrHide(boolean shown) {
        if (statusBarColorAnimator != null) {
            statusBarColorAnimator.cancel();
        }
        statusBarColorAnimator = ObjectAnimator.ofInt(
                (drawerLayout != null) ? drawerItemsListContainer : lUtils,
                (drawerLayout != null) ? "statusBarBackgroundColor" : "statusBarColor",
                shown ? Color.BLACK : mNormalStatusBarColor,
                shown ? mNormalStatusBarColor : Color.BLACK)
                .setDuration(250);
        if (drawerLayout != null) {
            statusBarColorAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    ViewCompat.postInvalidateOnAnimation(drawerLayout);
                }
            });
        }
        statusBarColorAnimator.setEvaluator(ARGB_EVALUATOR);
        statusBarColorAnimator.start();

//        updateSwipeRefreshProgressBarTop();

        for (View view : hideableHeaderViews) {
            if (shown) {
                view.animate()
                        .translationY(0)
                        .alpha(1)
                        .setDuration(HEADER_HIDE_ANIM_DURATION)
                        .setInterpolator(new DecelerateInterpolator());
            } else {
                view.animate()
                        .translationY(-view.getBottom())
                        .alpha(0)
                        .setDuration(HEADER_HIDE_ANIM_DURATION)
                        .setInterpolator(new DecelerateInterpolator());
            }
        }
    }

//    @Override
//    public boolean canSwipeRefreshChildScrollUp() {
//        return false;
//    }
}
