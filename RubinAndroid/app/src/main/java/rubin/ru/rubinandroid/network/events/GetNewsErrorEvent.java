package rubin.ru.rubinandroid.network.events;

/**
 * Created by Bogdan Kornev
 * on 02.03.2015 at 16:18.
 */
public class GetNewsErrorEvent extends BaseErrorEvent {
    public GetNewsErrorEvent(int responseCode, String message) {
        super(responseCode, message);
    }
}
