package rubin.ru.rubinandroid.network.actions;

import android.content.Context;
import android.os.Parcelable;

/**
 * Created by Bogdan Kornev
 * on 01.02.2015 at 0:50.
 */
public interface Action extends Parcelable {
    public void execute();

    public void setContext(Context context);
}
