package rubin.ru.rubinandroid.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Bogdan Kornev
 * on 27.02.2015 at 16:49.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements Parcelable {
    private int id;
    private String email;
    private String login;
    @JsonProperty("profile")
    private String avatar;

    public User() {}

    public User(Parcel in) {
        id = in.readInt();
        email = in.readString();
        login = in.readString();
        avatar = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(email == null ? "" : email);
        dest.writeString(login == null ? "" : login);
        dest.writeString(avatar == null ? "" : avatar);
    }

    public static Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) { return new User(source); }

        @Override
        public User[] newArray(int size) { return new User[0]; }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
