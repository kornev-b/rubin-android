package rubin.ru.rubinandroid.ui.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import rubin.ru.rubinandroid.Config;
import rubin.ru.rubinandroid.R;
import rubin.ru.rubinandroid.model.NewsItem;
import rubin.ru.rubinandroid.util.DateConvertUtils;
import rubin.ru.rubinandroid.util.SystemUtils;

/**
 * Created by Bogdan Kornev
 * on 03.03.2015 at 17:13.
 */
public class NewsAdapter extends BaseRecyclerViewAdapter {
    private ArrayList<NewsItem> news;

    public NewsAdapter(ClickListener clickListener) {
        super(clickListener);
        news = new ArrayList<>();
    }

    public void setNews(ArrayList<NewsItem> news) {
        this.news = news;
        onNewDataAppeared();
        notifyDataSetChanged();
    }

    public void addNewsToEnd(ArrayList<NewsItem> news) {
        this.news.addAll(news);
        onNewDataAppeared();
        notifyItemRangeInserted(getItemCount() - news.size() - 1, news.size());
    }

    public void addNewsToStart(ArrayList<NewsItem> news) {
        this.news.addAll(0, news);
        onNewDataAppeared();
        notifyItemRangeInserted(showActionBarViewOffset ? 1 : 0, news.size());
    }

    private void onNewDataAppeared() {
        isDataEmpty = news.size() == 0;
        isNetworkError = false;
        isProgressView = false;
        isPaginationInProgress = false;
    }

    @Override
    protected int getDefaultItemsCount() {
        return news.size();
    }

    @Override
    protected RecyclerView.ViewHolder onCreateDefaultViewHolder(ViewGroup parent, int viewType) {
        CardView cardView = (CardView) LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_news, parent, false);
        return new NewsViewHolder(cardView);
    }

    @Override
    protected String defineTextForEmptyData() {
        return recyclerView.getContext().getString(R.string.no_news);
    }

    @Override
    protected void onBindDefaultViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof NewsViewHolder) {
            Context context = recyclerView.getContext();
            NewsViewHolder newsViewHolder = (NewsViewHolder) holder;
            NewsItem item = news.get(position);

            int screenWidth = SystemUtils.getDisplayWidth(context);
            int spanCount = getSpanCount();
            int margins = (int) (context.getResources().getDimension(R.dimen.margin_small) * spanCount);
            int width = screenWidth / spanCount - margins * 2;
            int height = width * 2 / 3;
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width, height);
            newsViewHolder.image.setLayoutParams(lp);

            Picasso.with(context)
                    .load(Config.RUBIN_NEWS_IMAGES_PREFIX + item.getImage())
                    .tag(context)
                    .into(newsViewHolder.image);

            newsViewHolder.textViewSubject.setText(Html.fromHtml(item.getSubject()));
            newsViewHolder.textViewDate.setText(
                    DateConvertUtils.getUpdatedDateAgo(item.getConvertedDate()));
        }
    }

    private int getSpanCount() {
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof GridLayoutManager) {
            return ((GridLayoutManager) layoutManager).getSpanCount();
        } else if (layoutManager instanceof StaggeredGridLayoutManager) {
            return ((StaggeredGridLayoutManager) layoutManager).getSpanCount();
        }

        return 1;
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    private class NewsViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView textViewDate;
        TextView textViewSubject;

        public NewsViewHolder(CardView cardView) {
            super(cardView);
            image = (ImageView) cardView.findViewById(R.id.imageNews);
            textViewDate = (TextView) cardView.findViewById(R.id.textViewDate);
            textViewSubject = (TextView) cardView.findViewById(R.id.textViewSubject);
        }
    }
}
