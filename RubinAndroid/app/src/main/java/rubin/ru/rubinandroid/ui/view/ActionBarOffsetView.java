package rubin.ru.rubinandroid.ui.view;

import android.content.Context;
import android.content.res.Configuration;
import android.util.AttributeSet;
import android.view.View;

import rubin.ru.rubinandroid.util.SystemUtils;
import rubin.ru.rubinandroid.util.UIUtils;

/**
 * Created by Bogdan Kornev
 * on 20.02.2015 at 16:04.
 */
public class ActionBarOffsetView extends View {
    public ActionBarOffsetView(Context context) {
        super(context);
    }

    public ActionBarOffsetView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ActionBarOffsetView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int addition = (int) (8 * getContext().getResources().getDisplayMetrics().density);

        setMeasuredDimension(SystemUtils.getDisplayWidth(getContext()),
                UIUtils.calculateActionBarSize(getContext()) +
                        (SystemUtils.getScreenOrientation() == Configuration.ORIENTATION_LANDSCAPE ?
                                addition : 0));
    }
}
