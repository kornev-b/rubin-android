package rubin.ru.rubinandroid.network.actions;

import android.content.Context;
import android.os.Parcel;

import de.greenrobot.event.EventBus;
import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import rubin.ru.rubinandroid.network.RubinRest;
import rubin.ru.rubinandroid.network.SessionRestManager;
import rubin.ru.rubinandroid.network.events.NetworkErrorEvent;
import rubin.ru.rubinandroid.util.LogUtils;

/**
 * Created by Bogdan Kornev
 * on 02.02.2015 at 0:13.
 */
public abstract class BaseAction implements Action {
    protected Context context;
    protected final String TAG;
    {
        TAG = getClass().getSimpleName();
    }

    @Override
    public void setContext(Context context) {
        this.context = context.getApplicationContext();
    }

    protected RubinRest getRest() {
        return SessionRestManager.getInstance().getRest();
    }

    protected void processError(RetrofitError error) {
        switch (error.getKind()) {
            case NETWORK:
                EventBus.getDefault().post(new NetworkErrorEvent());
                break;
            case HTTP:
                onHttpError(error);
                break;
            case CONVERSION:
            case UNEXPECTED:
                LogUtils.LOGE(TAG, "Rertrofit error: " + error.getKind() +
                        (error.getMessage() != null ? error.getMessage() : ""));
                break;
        }
    }

    protected abstract void onHttpError(RetrofitError error);
}
