package rubin.ru.rubinandroid.ui.adapter;

import android.content.res.Configuration;
import android.graphics.Color;
import android.nfc.Tag;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import rubin.ru.rubinandroid.R;
import rubin.ru.rubinandroid.util.LogUtils;
import rubin.ru.rubinandroid.util.SystemUtils;

/**
 * Created by Bogdan Kornev
 * on 09.02.2015 at 19:55.
 */
public abstract class BaseRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    protected static final int TYPE_ITEM_VIEW = 0;
    protected static final int TYPE_EMPTY_VIEW = 1;
    protected static final int TYPE_NETWORK_ERROR_VIEW = 2;
    protected static final int TYPE_PROGRESS_VIEW = 3;
    protected static final int TYPE_ACTION_BAR_OFFSET_VIEW = 4;
    protected static final int TYPE_FOOTER_PAGINATION_PROGRESS = 5;
    protected final String TAG;

    protected boolean isDataEmpty;
    protected boolean isNetworkError;
    protected boolean isProgressView;
    protected boolean isPaginationInProgress;

    protected RecyclerView recyclerView;
    protected ClickListener clickListener;

    protected String filter;
    protected boolean showActionBarViewOffset;

    {
        TAG = getClass().getSimpleName();
    }

    public BaseRecyclerViewAdapter(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
        setSpanSizeLookupIfNeeded();
    }

    private void setSpanSizeLookupIfNeeded() {
        final RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof GridLayoutManager) {
            ((GridLayoutManager) layoutManager).setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (position == 0 && showActionBarViewOffset) {
                        return ((GridLayoutManager) layoutManager).getSpanCount();
                    }
                    return 1;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (isDataEmpty || isNetworkError || isProgressView) {
            return showActionBarViewOffset ? 2 : 1;
        }
        return getDefaultItemsCount() +
                (showActionBarViewOffset ? 1 : 0) +
                (isPaginationInProgress ? 1 : 0);
    }

    private int addNeededAccordingToLayoutManager() {
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof GridLayoutManager) {
            return ((GridLayoutManager) layoutManager).getSpanCount();
        } else if (layoutManager instanceof StaggeredGridLayoutManager) {
            return ((StaggeredGridLayoutManager) layoutManager).getSpanCount();
        }

        return 1;
    }

    protected abstract int getDefaultItemsCount();

    @Override
    public int getItemViewType(int position) {
        if (showActionBarViewOffset && position == 0) {
            return TYPE_ACTION_BAR_OFFSET_VIEW;
        }
        if (isDataEmpty) {
            return TYPE_EMPTY_VIEW;
        } else if (isNetworkError) {
            return TYPE_NETWORK_ERROR_VIEW;
        } else if (isProgressView) {
            return TYPE_PROGRESS_VIEW;
        } else if (position == getItemCount() - 1 && isPaginationInProgress) {
            return TYPE_FOOTER_PAGINATION_PROGRESS;
        }

        return TYPE_ITEM_VIEW;
    }

    private boolean checkIsActionBarOffsetPosition(int position) {
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof GridLayoutManager) {
            return position < ((GridLayoutManager) layoutManager).getSpanCount();
        } else if (layoutManager instanceof StaggeredGridLayoutManager) {
            return position < ((StaggeredGridLayoutManager) layoutManager).getSpanCount();
        } else if (position == 0) {
            return true;
        }

        return false;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_EMPTY_VIEW:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_data, parent, false);
                view.setLayoutParams(new RecyclerView.LayoutParams(
                        recyclerView.getWidth(), recyclerView.getHeight()));
                return new EmptyViewHolder(view);
            case TYPE_NETWORK_ERROR_VIEW:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_error_network, parent, false);
                view.setLayoutParams(new RecyclerView.LayoutParams(
                        recyclerView.getWidth(), recyclerView.getHeight()));
                return new NetworkErrorViewHolder(view);
            case TYPE_PROGRESS_VIEW:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_bar, parent, false);
                view.setLayoutParams(new RecyclerView.LayoutParams(
                        recyclerView.getWidth(), recyclerView.getHeight()));
                return new ProgressViewHolder(view);
            case TYPE_ACTION_BAR_OFFSET_VIEW:
//                view = new ActionBarOffsetView(parent.getContext());
                view = new View(recyclerView.getContext());
                view.setBackgroundColor(Color.TRANSPARENT);
                return new ActionBarOffsetViewHolder(view);
            case TYPE_FOOTER_PAGINATION_PROGRESS:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_progress_bar, parent, false);
                return new ProgressBarFooterViewHolder(view);
            default:
                return onCreateDefaultViewHolder(parent, viewType);
        }
    }

    protected abstract RecyclerView.ViewHolder onCreateDefaultViewHolder(ViewGroup parent, int viewType);

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof EmptyViewHolder) {
            EmptyViewHolder emptyViewHolder = (EmptyViewHolder) holder;
            emptyViewHolder.textViewNoData.setText(defineTextForEmptyData());
        } else if (holder instanceof NetworkErrorViewHolder) {
            NetworkErrorViewHolder networkErrorViewHolder = (NetworkErrorViewHolder) holder;
            networkErrorViewHolder.btnReload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListener != null) {
                        clickListener.click(v, holder.getPosition());
                    }
                }
            });
            networkErrorViewHolder.textViewNetworkError.setText(R.string.connection_problem_warning);
        } else if (holder instanceof ActionBarOffsetViewHolder) {
            setFullSpanIfNeeded(holder.itemView);
        } else if (holder instanceof ProgressBarFooterViewHolder) {

        } else if (holder instanceof ProgressViewHolder) {

        } else {
            onBindDefaultViewHolder(holder, holder.getPosition());
        }
    }

    private void setFullSpanIfNeeded(View itemView) {
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof StaggeredGridLayoutManager) {
            int addition = (int) (8 * recyclerView.getContext().getResources().getDisplayMetrics().density);

            StaggeredGridLayoutManager.LayoutParams lp =
                    new StaggeredGridLayoutManager.LayoutParams(
                            SystemUtils.getDisplayWidth(recyclerView.getContext()),
                            (SystemUtils.getScreenOrientation() == Configuration.ORIENTATION_LANDSCAPE ?
                                    addition : 0));
            itemView.setLayoutParams(lp);
            lp.setFullSpan(true);
        }
    }

    protected abstract String defineTextForEmptyData();

    protected abstract void onBindDefaultViewHolder(RecyclerView.ViewHolder holder, int position);

    public void showEmptyDataView() {
        isDataEmpty = true;
        isNetworkError = false;
        isProgressView = false;
        isPaginationInProgress = false;
        notifyDataSetChanged();
    }

    public void showNetworkErrorView() {
        isDataEmpty = false;
        isNetworkError = true;
        isProgressView = false;
        isPaginationInProgress = false;
        notifyDataSetChanged();
    }

    public void removeStateViews() {
        if (isDataEmpty || isNetworkError || isProgressView) {
            isDataEmpty = false;
            isNetworkError = false;
            isProgressView = false;
            isPaginationInProgress = false;
            notifyDataSetChanged();
        }
    }

    public void showProgressView() {
        isProgressView = true;
        isDataEmpty = false;
        isNetworkError = false;
        isPaginationInProgress = false;
        notifyDataSetChanged();
    }

    public void removeProgressView() {
        if (isProgressView) {
            isProgressView = false;
            notifyDataSetChanged();
        }
    }

    public void enablePaginationView(boolean enable) {
        if (isPaginationInProgress == enable) {
            return;
        }
        isPaginationInProgress = enable;
        LogUtils.LOGD(TAG, "Layout manager items count: " + recyclerView.getLayoutManager().getItemCount());
        LogUtils.LOGD(TAG, "Adapter items count: " + recyclerView.getLayoutManager().getItemCount());
        if (enable) {
            notifyItemInserted(getItemCount());
        } else {
            notifyItemRemoved(getItemCount());
        }
    }

    public void enableFakeActionBarViewOffset(boolean enable) {
        showActionBarViewOffset = enable;
        notifyDataSetChanged();
    }

    public boolean isProgressShowing() {
        return isProgressView;
    }

    protected class EmptyViewHolder extends RecyclerView.ViewHolder {
        TextView textViewNoData;
        ImageView imageViewEmpty;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            textViewNoData = (TextView) itemView.findViewById(R.id.textViewNoData);
            imageViewEmpty = (ImageView) itemView.findViewById(R.id.imageViewEmpty);
        }
    }

    protected class NetworkErrorViewHolder extends RecyclerView.ViewHolder {
        TextView textViewNetworkError;
        ImageView imageViewNetworkError;
        Button btnReload;

        public NetworkErrorViewHolder(View itemView) {
            super(itemView);
            textViewNetworkError = (TextView) itemView.findViewById(R.id.textViewNetworkError);
            imageViewNetworkError = (ImageView) itemView.findViewById(R.id.imageViewNetworkError);
            btnReload = (Button) itemView.findViewById(R.id.btnReload);
        }
    }

    protected class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressViewHolder(View itemView) { super(itemView); }
    }

    private class ActionBarOffsetViewHolder extends RecyclerView.ViewHolder {
        public ActionBarOffsetViewHolder(View view) { super(view); }
    }

    private class ProgressBarFooterViewHolder extends RecyclerView.ViewHolder {
        public ProgressBarFooterViewHolder(View itemView) { super(itemView); }
    }

    public interface ClickListener {
        public void click(View view, int position);
    }
}
