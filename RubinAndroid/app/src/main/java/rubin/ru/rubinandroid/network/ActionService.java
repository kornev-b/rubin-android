package rubin.ru.rubinandroid.network;

import android.app.IntentService;
import android.content.Intent;

import rubin.ru.rubinandroid.network.actions.Action;
import rubin.ru.rubinandroid.util.Constants;

/**
 * Created by Bogdan Kornev
 * on 11.01.2015 at 16:29.
 */
public class ActionService extends IntentService {
    private static final String TAG = ActionService.class.getSimpleName();

    public ActionService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Action action = intent.getParcelableExtra(Constants.ACTION_EXTRA);
        action.setContext(getApplicationContext());
        action.execute();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
