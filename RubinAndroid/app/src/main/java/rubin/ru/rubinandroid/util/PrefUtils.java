package rubin.ru.rubinandroid.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import rubin.ru.rubinandroid.model.User;

public class PrefUtils {
    private static final String TAG = LogUtils.makeLogTag("PrefUtils");

    public static final String PREF_FIRST_LAUNCH = "pref_first_launch";
    public static final String PREF_AUTH_DATA = "pref_auth_data";
    public static final String PREF_USER = "pref_user";

    public static boolean isFirstLaunch(final Context context) {
        if (context == null) {
            return false;
        }
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        if (!sp.contains(PREF_FIRST_LAUNCH)) {
            sp.edit().putBoolean(PREF_FIRST_LAUNCH, false).apply();
            return true;
        }
        return false;
    }

    public static boolean isSignedIn(final Context context) {
        if (context == null) {
            return false;
        }
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        return sp.contains(PREF_AUTH_DATA);
    }

    public static void saveAuthToken(final Context context, String authToken) {
        if (context == null) {
            return;
        }
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        sp.edit().putString(PREF_AUTH_DATA, authToken)
                .apply();
    }

    public static String getAuthToken(final Context context) {
        if (context == null) {
            return null;
        }
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        return sp.getString(PREF_AUTH_DATA, "");
    }

    public static void registerOnSharedPreferenceChangeListener(final Context context,
            SharedPreferences.OnSharedPreferenceChangeListener listener) {
        if (context == null) {
            return;
        }
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        sp.registerOnSharedPreferenceChangeListener(listener);
    }

    public static void unregisterOnSharedPreferenceChangeListener(final Context context,
            SharedPreferences.OnSharedPreferenceChangeListener listener) {
        if (context == null) {
            return;
        }
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        sp.unregisterOnSharedPreferenceChangeListener(listener);
    }

    public static void removeAuthToken(final Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        sp.edit().remove(PREF_AUTH_DATA).commit();
    }

    public static void clearPreferences(final Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        sp.edit()
                .remove(PREF_FIRST_LAUNCH)
                .remove(PREF_AUTH_DATA)
                .commit();
    }

    public static void saveUser(final Context context, User user) {
        if (context == null) {
            return;
        }
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        ObjectMapper om = new ObjectMapper();
        try {
            String json = om.writer().writeValueAsString(user);
            sp.edit().putString(PREF_USER, json).commit();
        } catch (JsonProcessingException e) {
            LogUtils.LOGE(TAG, e.getMessage(), e.getCause());
        }
    }

    public static User getUser(final Context context) {
        if (context == null) {
            return null;
        }
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        ObjectMapper om = new ObjectMapper();
        String json = sp.getString(PREF_USER, "");
        if (!TextUtils.isEmpty(json)) {
            try {
                return om.readValue(json, User.class);
            } catch (IOException e) {
                LogUtils.LOGE(TAG, e.getMessage(), e.getCause());
            }
        }
        return null;
    }
}
