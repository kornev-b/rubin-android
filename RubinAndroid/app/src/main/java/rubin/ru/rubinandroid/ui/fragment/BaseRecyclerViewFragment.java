package rubin.ru.rubinandroid.ui.fragment;

import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;
import rubin.ru.rubinandroid.R;
import rubin.ru.rubinandroid.network.SessionRestManager;
import rubin.ru.rubinandroid.network.events.UnauthorizedRequestErrorEvent;
import rubin.ru.rubinandroid.ui.BaseActivity;
import rubin.ru.rubinandroid.ui.adapter.BaseRecyclerViewAdapter;
import rubin.ru.rubinandroid.util.UIUtils;

/**
 * Created by Bogdan Kornev
 * on 09.02.2015 at 19:41.
 */
public abstract class BaseRecyclerViewFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
        BaseRecyclerViewAdapter.ClickListener, LoaderManager.LoaderCallbacks<Cursor> {
    private final static String LAST_FVI_KEY = "lastFvi";
    private final static int ITEMS_THRESHOLD = 3;
    int lastFvi = 0;

    protected final String TAG;
    private Map<ContentObserver, Uri> contentObservers = new HashMap<>();

    protected SwipeRefreshLayout swipeLayout;
    protected RecyclerView recyclerView;

    {
        TAG = getClass().getSimpleName();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (null != savedInstanceState) {
            lastFvi = savedInstanceState.getInt(LAST_FVI_KEY);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(LAST_FVI_KEY, lastFvi);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        View root = getView();
        if (root == null) {
            return;
        }
        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        swipeLayout = (SwipeRefreshLayout) getView().findViewById(R.id.refreshLayout);
        if (swipeLayout != null) {
            swipeLayout.setOnRefreshListener(this);

            swipeLayout.setColorSchemeResources(
                    R.color.refresh_progress_1,
                    R.color.refresh_progress_2,
                    R.color.refresh_progress_3);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        for (Map.Entry<ContentObserver, Uri> entry : contentObservers.entrySet()) {
            getActivity().getContentResolver().registerContentObserver(
                    entry.getValue(), true, entry.getKey());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
        for (Map.Entry<ContentObserver, Uri> entry : contentObservers.entrySet()) {
            getActivity().getContentResolver().unregisterContentObserver(entry.getKey());
        }
    }

    protected void addContentObserver(ContentObserver contentObserver, Uri uri) {
        contentObservers.put(contentObserver, uri);
    }

    protected void cancelRefreshing() {
        if (swipeLayout.isRefreshing()) {
            swipeLayout.setRefreshing(false);
        }
    }

    public void onEventMainThread(UnauthorizedRequestErrorEvent event) {
        SessionRestManager.getInstance().logout(getActivity());
    }

    protected void enableSwipeLayout(boolean enable) {
        swipeLayout.setEnabled(enable);
    }

    protected void onMainContentScrolled() {
        if (getActivity() instanceof BaseActivity) {
            int firstVisibleItem = getFirstVisibleItem(recyclerView);
            ((BaseActivity) getActivity()).onMainContentScrolled(
                    firstVisibleItem <= ITEMS_THRESHOLD ? 0 : Integer.MAX_VALUE,
                    lastFvi - firstVisibleItem > 0 ? Integer.MIN_VALUE :
                            lastFvi == firstVisibleItem ? 0 : Integer.MAX_VALUE
            );
            lastFvi = firstVisibleItem;
        }
    }

    protected void enableActionBarAutoHide() {
        if (swipeLayout != null) {
            int abHeight = UIUtils.calculateActionBarSize(getActivity());
            swipeLayout.setProgressViewOffset(false, abHeight, abHeight * 2);
        }
    }

    private int getFirstVisibleItem(RecyclerView recyclerView) {
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof LinearLayoutManager) {
            return ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
        }
        return ((StaggeredGridLayoutManager) layoutManager).findFirstVisibleItemPositions(null)[0];
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }
}
