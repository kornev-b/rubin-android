package rubin.ru.rubinandroid.network;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import rubin.ru.rubinandroid.BuildConfig;
import rubin.ru.rubinandroid.ui.StartActivity;
import rubin.ru.rubinandroid.util.JacksonConverter;
import rubin.ru.rubinandroid.util.LogUtils;
import rubin.ru.rubinandroid.util.PrefUtils;
import rubin.ru.rubinandroid.util.SystemUtils;

public class SessionRestManager {
    private static final String TAG = SessionRestManager.class.getSimpleName();
    private static SessionRestManager ourInstance = new SessionRestManager();
    private Context context;

    public static SessionRestManager getInstance() {
        return ourInstance;
    }

    private SessionRestManager() {}

    /**
     * Injects basic auth header to an every request
     */
    private final RequestInterceptor REQUEST_INTERCEPTOR = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade request) {
            if (PrefUtils.isSignedIn(context)) {
                request.addQueryParam("auth_token", getAuthToken());
            }
            request.addHeader("Accept", "application/json");
        }
    };

    private final RestAdapter.Builder REST_ADAPTER_BUILDER = new RestAdapter.Builder()
            .setEndpoint(SystemUtils.getRestEndpoint())
            .setConverter(new JacksonConverter())
            .setRequestInterceptor(REQUEST_INTERCEPTOR)
            .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL
                    : RestAdapter.LogLevel.NONE);

    private final RestAdapter REST_ADAPTER = REST_ADAPTER_BUILDER.build();

    public RubinRest getRest() {
        return REST_ADAPTER.create(RubinRest.class);
    }

    public void logout(Activity activity) {
        PrefUtils.clearPreferences(activity);
        SystemUtils.stopService(activity, ActionService.class);
        cleanDB();
        Intent intent = new Intent(activity, StartActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }

    private String getAuthToken() {
        if (context == null) {
            throw new IllegalStateException("Context is not set in the SessionManager");
        }

        return PrefUtils.getAuthToken(context);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void cleanDB() {
        LogUtils.LOGD(TAG, "Logout: cleaning db...");
        LogUtils.LOGD(TAG, "Cleaning table ...");
//        int deletedRecords = context.getContentResolver().delete(
//                GitHubContract.ReposTable.CONTENT_URI,
//                null,
//                null
//        );
//        LogUtils.LOGD(TAG, "deleted records: " + deletedRecords);
    }
}
