package rubin.ru.rubinandroid.provider;

import android.content.ContentUris;
import android.net.Uri;

/**
 * Created by Bogdan Kornev
 * on 27.02.2015 at 16:39.
 */
public class RubinContract {
    public static final String CONTENT_AUTHORITY = "ru.rubin.rubinandroid";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    interface NewsColumns {
        String ID = "id";
        String BODY = "body";
        String SUBJECT = "subject";
        String PUBLISHED = "published";
        String IMAGE = "image";
    }

    public static final String PATH_NEWS = "news";

    public static class NewsTable implements NewsColumns {
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_NEWS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_NEWS;

        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_NEWS;

        public static final String TABLE_NAME = "news";

        public static Uri buildReposURI(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}
