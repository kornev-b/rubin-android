package rubin.ru.rubinandroid.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import rubin.ru.rubinandroid.R;
import rubin.ru.rubinandroid.util.PrefUtils;

/**
 * Starting activity. Launches Auth, Home or SplashScreen depending on conditions.
 */
public class StartActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        Intent intent;
//        if (!PrefUtils.isSignedIn(this)) {
//            intent = new Intent(this, SignInActivity.class);
//        } else {
//            intent = new Intent(this, NewsActivity.class);
//        }
        intent = new Intent(this, NewsActivity.class);
        startActivity(intent);
        finish();
    }
}