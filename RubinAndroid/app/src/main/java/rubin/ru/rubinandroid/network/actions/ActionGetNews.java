package rubin.ru.rubinandroid.network.actions;

import android.content.ContentValues;
import android.os.Parcel;
import android.text.Html;

import de.greenrobot.event.EventBus;
import retrofit.RetrofitError;
import rubin.ru.rubinandroid.model.NewsItem;
import rubin.ru.rubinandroid.network.events.GetNewsEmptyListEvent;
import rubin.ru.rubinandroid.network.events.GetNewsErrorEvent;
import rubin.ru.rubinandroid.provider.RubinContract;
import rubin.ru.rubinandroid.util.DateConvertUtils;
import rubin.ru.rubinandroid.util.LogUtils;

/**
 * Created by Bogdan Kornev
 * on 02.03.2015 at 14:56.
 */
public class ActionGetNews extends BaseAction {
    private int page;

    public ActionGetNews() {
    }

    public ActionGetNews(int page) {
        this.page = page;
    }

    public ActionGetNews(Parcel in) {
        page = in.readInt();
    }

    @Override
    public void execute() {
        if (context == null) {
            LogUtils.LOGE(TAG, "Context is not set!");
            return;
        }

        NewsItem.News news = null;

        try {
            if (page == 0) {
                news = getRest().getNews(1);
            } else {
                news = getRest().getNews(page);
            }
        } catch (RetrofitError error) {
            processError(error);
        }

        if (news != null) {
            if (news.size() > 0) {
                ContentValues[] cvArray = new ContentValues[news.size()];
                for (int i = 0; i < news.size(); i++) {
                    cvArray[i] = mapNewsItemToContentValues(news.get(i));
                }
                context.getContentResolver()
                        .bulkInsert(RubinContract.NewsTable.CONTENT_URI, cvArray);
            } else {
                EventBus.getDefault().post(new GetNewsEmptyListEvent());
//                cleanNewsTable();
            }
        }
    }

    private void cleanNewsTable() {
        LogUtils.LOGD(TAG, "Received empty list of news from server. Cleaning table News...");
        int deletedRecords = context.getContentResolver().delete(
                RubinContract.NewsTable.CONTENT_URI,
                null,
                null
        );
        LogUtils.LOGD(TAG, "deleted records: " + deletedRecords);
    }

    private ContentValues mapNewsItemToContentValues(NewsItem newsItem) {
        ContentValues cv = new ContentValues();
        cv.put(RubinContract.NewsTable.ID, newsItem.getId());
        cv.put(RubinContract.NewsTable.BODY, Html.fromHtml(newsItem.getBody()).toString());
        cv.put(RubinContract.NewsTable.SUBJECT, Html.fromHtml(newsItem.getSubject()).toString());
        cv.put(RubinContract.NewsTable.PUBLISHED, newsItem.getConvertedDate());
        cv.put(RubinContract.NewsTable.IMAGE, newsItem.getImage());
        return cv;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(page);
    }

    public static Creator<ActionGetNews> CREATOR = new Creator<ActionGetNews>() {
        @Override
        public ActionGetNews createFromParcel(Parcel source) {
            return new ActionGetNews(source);
        }

        @Override
        public ActionGetNews[] newArray(int size) {
            return new ActionGetNews[0];
        }
    };

    @Override
    protected void onHttpError(RetrofitError error) {
        EventBus.getDefault().post(new GetNewsErrorEvent(
                (error.getResponse() != null ? error.getResponse().getStatus() : 0), error.getMessage()));
    }
}
