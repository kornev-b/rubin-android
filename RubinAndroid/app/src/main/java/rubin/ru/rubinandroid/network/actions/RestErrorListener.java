package rubin.ru.rubinandroid.network.actions;

import retrofit.RetrofitError;

/**
 * Created by Bogdan Kornev
 * on 02.03.2015 at 15:35.
 */
public interface RestErrorListener {
    public void onError(RetrofitError error);
}
