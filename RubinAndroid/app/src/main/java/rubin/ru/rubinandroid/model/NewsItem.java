package rubin.ru.rubinandroid.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

import rubin.ru.rubinandroid.util.DateConvertUtils;

/**
 * Created by Bogdan Kornev
 * on 02.03.2015 at 14:16.
 */
@JsonIgnoreProperties({"convertedDate"})
public class NewsItem implements Parcelable {
    private long id;
    private String body;
    private String subject;
    private String published;
    private String image;
    private long convertedDate;

    public NewsItem() {}

    public NewsItem(Parcel in) {
        id = in.readLong();
        body = in.readString();
        subject = in.readString();
        published = in.readString();
        image = in.readString();
        convertedDate = in.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(body == null ? "" : body);
        dest.writeString(subject == null ? "" : subject);
        dest.writeString(published);
        dest.writeString(image == null ? "" : image);
        dest.writeLong(convertedDate);
    }

    public static Creator<NewsItem> CREATOR = new Creator<NewsItem>() {
        @Override
        public NewsItem createFromParcel(Parcel source) { return new NewsItem(source); }

        @Override
        public NewsItem[] newArray(int size) { return new NewsItem[0]; }
    };

    public static class News extends ArrayList<NewsItem> {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
        convertedDate = DateConvertUtils.getDateInMillisForRubinDate(published);
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getConvertedDate() {
        return convertedDate;
    }
}
