package rubin.ru.rubinandroid.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Bogdan Kornev
 * on 27.02.2015 at 16:40.
 */
public class RubinDbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "githubclient.db";

    public RubinDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createNewsTable(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys = ON");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + RubinContract.NewsTable.TABLE_NAME);
        onCreate(db);
    }

    private void createNewsTable(SQLiteDatabase db) {
        final String sql =
                "CREATE TABLE " + RubinContract.NewsTable.TABLE_NAME + " (" +
                        RubinContract.NewsTable.ID + " INTEGER PRIMARY KEY NOT NULL, " +
                        RubinContract.NewsTable.BODY + " TEXT, " +
                        RubinContract.NewsTable.SUBJECT + " TEXT, " +
                        RubinContract.NewsTable.PUBLISHED + " INTEGER, " +
                        RubinContract.NewsTable.IMAGE + " TEXT);";
        db.execSQL(sql);
    }
}
