package rubin.ru.rubinandroid;

import android.app.Application;

import rubin.ru.rubinandroid.network.SessionRestManager;

/**
 * Created by Bogdan Kornev
 * on 27.01.2015 at 13:29.
 */
public class OurApp extends Application {
    public OurApp() {
        SessionRestManager sessionRestManager = SessionRestManager.getInstance();
        sessionRestManager.setContext(this);
//        Ln.getConfig().setLoggingLevel(Log.ERROR);
    }
}
