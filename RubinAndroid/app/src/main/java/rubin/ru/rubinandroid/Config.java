package rubin.ru.rubinandroid;

public class Config {
    public static final String RUBIN_ENDPOINT_DEBUG = "http://rubin-kazan.ru/api/v1";
    public static final String RUBIN_ENDPOINT_RELEASE = RUBIN_ENDPOINT_DEBUG;
    public static final String RUBIN_IMAGES_URL_PREFIX = "http://rubin-kazan.ru/";
    public static final String RUBIN_NEWS_IMAGES_PREFIX = "http://rubin-kazan.ru";

    public static final int RESPONSE_NETWORK_UNAVAILABLE = 1001;
    public static final int RESPONSE_NOT_FOUND = 404;
    public static final int RESPONSE_UNAUTHORIZED = 401;
}
