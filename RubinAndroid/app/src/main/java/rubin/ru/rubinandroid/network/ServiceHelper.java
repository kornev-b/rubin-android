package rubin.ru.rubinandroid.network;

import android.app.Activity;
import android.content.Intent;

import rubin.ru.rubinandroid.network.actions.Action;
import rubin.ru.rubinandroid.util.Constants;
import rubin.ru.rubinandroid.util.UIUtils;

/**
 * Created by Bogdan Kornev
 * on 27.01.2015 at 13:31.
 */
public class ServiceHelper {
    private static ServiceHelper ourInstance = new ServiceHelper();

    public static ServiceHelper getInstance() {
        return ourInstance;
    }

    private ServiceHelper() {
    }

    public void startActionService(Activity activity, Action action) {
        UIUtils.hideSoftKeyboard(activity);
        Intent intent = new Intent(activity, ActionService.class);
        intent.putExtra(Constants.ACTION_EXTRA, action);
        activity.startService(intent);
    }
}
