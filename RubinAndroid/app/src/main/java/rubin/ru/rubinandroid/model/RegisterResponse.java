package rubin.ru.rubinandroid.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Bogdan Kornev
 * on 27.02.2015 at 18:37.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterResponse implements Parcelable {
    private boolean success;
    private String info;

    public RegisterResponse() {}

    public RegisterResponse(Parcel in) {
        success = in.readInt() == 1;
        info = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(success ? 1 : 0);
        dest.writeString(info == null ? "" : info);
    }

    public static Creator<RegisterResponse> CREATOR = new Creator<RegisterResponse>() {
        @Override
        public RegisterResponse createFromParcel(Parcel source) { return new RegisterResponse(source); }

        @Override
        public RegisterResponse[] newArray(int size) { return new RegisterResponse[0]; }
    };

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static class Data implements Parcelable {
        private User user;
        @JsonProperty("auth_token")
        private String authToken;

        public Data() {}

        public Data(Parcel in) {
            user = in.readParcelable(User.class.getClassLoader());
            authToken = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(user == null ? new User() : user, flags);
            dest.writeString(authToken);
        }

        public static Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel source) { return new Data(source); }

            @Override
            public Data[] newArray(int size) { return new Data[0]; }
        };

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public String getAuthToken() {
            return authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }
    }
}
