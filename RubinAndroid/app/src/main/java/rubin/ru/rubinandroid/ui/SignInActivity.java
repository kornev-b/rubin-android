package rubin.ru.rubinandroid.ui;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import de.greenrobot.event.EventBus;
import rubin.ru.rubinandroid.BuildConfig;
import rubin.ru.rubinandroid.R;
import rubin.ru.rubinandroid.network.events.NetworkErrorEvent;
import rubin.ru.rubinandroid.network.events.UnauthorizedRequestErrorEvent;
import rubin.ru.rubinandroid.util.ConnectivityChecker;
import rubin.ru.rubinandroid.util.UIUtils;

public class SignInActivity extends Activity {
    public static final String AUTH_IN_PROGRESS_KEY = "authInProgress";
    public static final String SHOW_PASSWORD_KEY = "showPassword";

    private EditText username;
    private EditText password;
    private Button signInButton;

    private RelativeLayout progressBarWrapper;
    private ScrollView scrollView;

    // RetainInstance
    private boolean showPassword = false;
    private boolean isAuthInProgress = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        if (null != savedInstanceState) {
            isAuthInProgress = savedInstanceState.getBoolean(AUTH_IN_PROGRESS_KEY);
            showPassword = savedInstanceState.getBoolean(SHOW_PASSWORD_KEY);
        }

        // Init views
        // Login field
        username = (EditText)findViewById(R.id.username);
        username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                onFieldsTextChanged();
            }
        });

        // Password field
        password = (EditText) findViewById(R.id.password);

        Drawable d = getResources().getDrawable(R.drawable.ic_remove_red_eye_white_24dp);
        d.setColorFilter(R.color.bluegrey_400, PorterDuff.Mode.MULTIPLY);
        password.setCompoundDrawablesWithIntrinsicBounds(null, null, d, null);
        password.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (password.getRight() - password.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        showPassword = !showPassword;
                        showOrHidePassword();
                        return true;
                    }
                }
                return false;
            }
        });
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                onFieldsTextChanged();
            }
        });

        showOrHidePassword();

        // SignIn button
        signInButton = (Button) findViewById(R.id.signIn);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });

        // Misc
        progressBarWrapper = (RelativeLayout) findViewById(R.id.progressBarWrapper);
        scrollView = (ScrollView) findViewById(R.id.scrollView);

        if (BuildConfig.DEBUG) {
            username.setText("");
            password.setText("");
        }

        // Handling IME action (sign in)
        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == 6) {
                    signIn();
                    return true;
                }
                return false;
            }
        });

        if (isAuthInProgress) {
            showProgressBar();
            // todo check if user already signed in
        }
    }

    private void showOrHidePassword() {
        password.setInputType(showPassword ? InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD : (InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD));
        password.setSelection(password.length());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(AUTH_IN_PROGRESS_KEY, isAuthInProgress);
        outState.putBoolean(SHOW_PASSWORD_KEY, showPassword);
    }

    /**
     * Method disables/enables Login button depending on username/password fields
     */
    protected void onFieldsTextChanged() {
        signInButton.setEnabled(username.getText().length() > 0 && password.getText().length() > 0);
    }

    /**
     * Sign in method. Starts service with intent, containing login and password.
     * Also shows progressbar.
     */
    private void signIn() {
        if (ConnectivityChecker.isNetworkAvailable(this)) {
//            Action action = new GetUserAction(username.getText().toString(), password.getText().toString());
//            ServiceHelper.getInstance().startActionService(this, action);
            isAuthInProgress = true;
            showProgressBar();
        } else {
            UIUtils.showShortToast(this, R.string.reason_network_unavailable);
        }
    }

    private void showProgressBar() {
        scrollView.setVisibility(View.GONE);
        progressBarWrapper.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        progressBarWrapper.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

//    public void onEventMainThread(GetUserErrorEvent event) {
//        hideProgressBar();
//        Toast.makeText(this, "Failed to login.", Toast.LENGTH_SHORT).show();
//    }
//
//    public void onEventMainThread(GetUserSuccessEvent event) {
//        hideProgressBar();
//        Intent intent = new Intent(this, HomeActivity.class);
//        startActivity(intent);
//    }

    public void onEventMainThread(NetworkErrorEvent event) {
        hideProgressBar();
        Toast.makeText(this, R.string.reason_network_unavailable, Toast.LENGTH_SHORT).show();
    }

    public void onEventMainThread(UnauthorizedRequestErrorEvent event) {
        hideProgressBar();
        Toast.makeText(this, R.string.reason_bad_credentials, Toast.LENGTH_SHORT).show();
    }
}
