package rubin.ru.rubinandroid.ui.fragment;

import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import rubin.ru.rubinandroid.R;
import rubin.ru.rubinandroid.model.NewsItem;
import rubin.ru.rubinandroid.network.ServiceHelper;
import rubin.ru.rubinandroid.network.actions.Action;
import rubin.ru.rubinandroid.network.actions.ActionGetNews;
import rubin.ru.rubinandroid.network.events.GetNewsEmptyListEvent;
import rubin.ru.rubinandroid.network.events.GetNewsErrorEvent;
import rubin.ru.rubinandroid.network.events.NetworkErrorEvent;
import rubin.ru.rubinandroid.provider.RubinContract;
import rubin.ru.rubinandroid.ui.BaseActivity;
import rubin.ru.rubinandroid.ui.adapter.NewsAdapter;
import rubin.ru.rubinandroid.util.CursorHelper;
import rubin.ru.rubinandroid.util.LogUtils;

/**
 * Created by Bogdan Kornev
 * on 03.03.2015 at 16:21.
 */
public class NewsFragment extends BaseRecyclerViewFragment {
    private static final String NEWS_KEY = "news";
    private static final String LAST_PAGE_PAGINATION_KEY = "lastPaginationPage";
    private static final String LAST_PAGE_FRESH_NEWS = "lastPageFreshNews";
    private static final String LAST_FRESH_NEWS_DATE_KEY = "lastFreshNewsDate";
    private static final String LAST_OLD_NEWS_DATE_KEY = "lastOldNewsDate";
    private static final String IS_PROGRESS_BAR_SHOWING_KEY = "isProgressBarShowing";
    private static final String IS_LOADING_NEWS_KEY = "isLoadingNews";
    private static final String IS_PAGINATION_KEY = "isPagination";
    private static final String IS_REFRESHING_KEY = "isRefreshing";

    private static final int NEWS_LOADER = 0;

    private NewsAdapter newsAdapter;
    private AsyncNewsParser asyncNewsParser;

    // retain state
    private ArrayList<NewsItem> news;
    private int lastPaginationPage = 2;
    private int lastPageFreshNews = 1;
    private boolean isProgressBarShowing;
    private boolean isLoadingNews;
    private boolean isPagination;
    private boolean isRefreshing;
    private long lastFreshNewsDate;
    private long lastOldNewsDate;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_news, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Init Recycler View
        final StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(
                getResources().getInteger(R.integer.repos_columns_number), OrientationHelper.VERTICAL);
        layoutManager.setGapStrategy(
                StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            boolean isScrolling = false;

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                isScrolling = (newState != RecyclerView.SCROLL_STATE_IDLE
                        && newsAdapter.getItemCount() != 0);
                Picasso picasso = Picasso.with(getActivity());
                if (isScrolling) {
                    picasso.pauseTag(getActivity());
                } else {
                    picasso.resumeTag(getActivity());
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                enableSwipeLayout(!recyclerView.canScrollVertically(-1) ||
                        !recyclerView.canScrollVertically(1));
                if (isScrolling) {
                    int[] lastVisibleItems = new int[layoutManager.getSpanCount()];
                    layoutManager.findLastVisibleItemPositions(lastVisibleItems);
                    int totalItemCount = layoutManager.getItemCount();
                    int lastVisibleItem = lastVisibleItems[layoutManager.getSpanCount() - 1];

                    if (totalItemCount > 10 && lastVisibleItem > 0
                            && lastVisibleItem >= totalItemCount - 10) {
                        newsAdapter.enablePaginationView(true);
                        loadNextPageOfNews();
                    }
                }
            }
        });

        newsAdapter = new NewsAdapter(this);
        recyclerView.setAdapter(newsAdapter);

        if (null != savedInstanceState) {
            news = savedInstanceState.getParcelableArrayList(NEWS_KEY);
            lastPaginationPage = savedInstanceState.getInt(LAST_PAGE_PAGINATION_KEY);
            lastPageFreshNews = savedInstanceState.getInt(LAST_PAGE_FRESH_NEWS);
            isProgressBarShowing = savedInstanceState.getBoolean(IS_PROGRESS_BAR_SHOWING_KEY);
            isPagination = savedInstanceState.getBoolean(IS_PAGINATION_KEY);
            isLoadingNews = savedInstanceState.getBoolean(IS_LOADING_NEWS_KEY);
            isRefreshing = savedInstanceState.getBoolean(IS_REFRESHING_KEY);
            lastFreshNewsDate = savedInstanceState.getLong(LAST_FRESH_NEWS_DATE_KEY);
            lastOldNewsDate = savedInstanceState.getLong(LAST_OLD_NEWS_DATE_KEY);
            if (news != null) {
                newsAdapter.setNews(news);
            }
            if (isProgressBarShowing) {
                showProgressBar();
            }
            if (isRefreshing) {
                swipeLayout.setRefreshing(true);
            }
        }

        enableSwipeLayout(!recyclerView.canScrollVertically(-1) ||
                !recyclerView.canScrollVertically(1));

        setUpSmoothScroolOnToolbarTap();
    }

    private void loadNextPageOfNews() {
        if (isPagination) {
            return;
        }
        isPagination = true;
        ActionGetNews actionGetNews = new ActionGetNews(lastPaginationPage);
        lastPaginationPage++;
        ServiceHelper.getInstance().startActionService(getActivity(), actionGetNews);
    }

    private void setUpSmoothScroolOnToolbarTap() {
        if (getActivity() instanceof BaseActivity) {
            Toolbar toolbar = ((BaseActivity) getActivity()).getActionBarToolbar();
            toolbar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    smoothScrollToTop();
                }
            });
        }
    }

    private void smoothScrollToTop() {
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        addContentObserver(new NewsObserver(new Handler()), RubinContract.NewsTable.CONTENT_URI);
        // When user 1st time launches app we trying to load last cached news
        // if they exist
        if (null == savedInstanceState && null == news) {
            showProgressBar();
            getLoaderManager().initLoader(NEWS_LOADER, null, this);
        }
    }

    private void loadNewsFromNetwork() {
        if (isLoadingNews) {
            return;
        }

        isLoadingNews = true;
        Action action = new ActionGetNews(lastPageFreshNews);
        ServiceHelper.getInstance().startActionService(getActivity(), action);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (news == null && getLoaderManager().getLoader(NEWS_LOADER) == null) {
            restartLoader(NEWS_LOADER);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getLoaderManager().destroyLoader(NEWS_LOADER);
        if (asyncNewsParser != null && asyncNewsParser.getStatus() != AsyncTask.Status.FINISHED) {
            isLoadingNews = false;
            isPagination = false;
            isRefreshing = false;
            isProgressBarShowing = false;
            asyncNewsParser.cancel(true);
        }
    }

    private void showProgressBar() {
        isProgressBarShowing = true;
        newsAdapter.showProgressView();
    }

    @Override
    public void click(View view, int position) {
        // todo
    }

    public void onEventMainThread(GetNewsEmptyListEvent event) {
        if (isRefreshing) {
            isRefreshing = false;
            cancelRefreshing();
        }
        if (isLoadingNews) {
            hideProgressView();
            if (news == null) {
                newsAdapter.showEmptyDataView();
                Toast.makeText(getActivity(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
            }
        }
        if (isPagination) {
            cancelPagination();
        }
    }

    public void onEventMainThread(GetNewsErrorEvent event) {
        onLoadingNewsError();
        Toast.makeText(getActivity(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
    }

    public void onEventMainThread(NetworkErrorEvent event) {
        onLoadingNewsError();
        Toast.makeText(getActivity(), R.string.reason_network_unavailable, Toast.LENGTH_SHORT).show();
    }

    private void onLoadingNewsError() {
        if (isRefreshing) {
            isRefreshing = false;
            cancelRefreshing();
        }

        if (isLoadingNews) {
            hideProgressView();
            if (news == null || news.size() == 0) {
                newsAdapter.showNetworkErrorView();
            }
        }
        if (isPagination) {
            cancelPagination();
        }

        Toast.makeText(getActivity(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        isRefreshing = true;
        swipeLayout.setRefreshing(true);
        loadNewsFromNetwork();
    }

    private void restartLoader(int loaderId) {
        getLoaderManager().restartLoader(loaderId, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = RubinContract.NewsTable.CONTENT_URI;
        String selection = null;
        String[] selectionArgs = null;

        // If we need to distinguish between fresh and old news
        // we specifying that by the selection parameters
        // else we just loading all news providing nulls to the corresponding parameters.
        if (isLoadingNews) {
            selection = lastFreshNewsDate > 0 ? RubinContract.NewsTable.PUBLISHED + ">?" : null;
            selectionArgs = lastFreshNewsDate > 0 ? new String[]{"" + lastFreshNewsDate} : null;
        } else if (isPagination) {
            selection = lastOldNewsDate > 0 ? RubinContract.NewsTable.PUBLISHED + "<?" : null;
            selectionArgs = lastOldNewsDate > 0 ? new String[]{"" + lastOldNewsDate} : null;
        }

        // And we need to extract news from the db in the descending order by
        // publication date.
        return new CursorLoader(
                getActivity(),
                uri,
                null, // leaving "columns" null just returns all the columns
                selection, // cols for "where" clause
                selectionArgs, // values for "where" clause
                RubinContract.NewsTable.PUBLISHED + " DESC"  // values for "order" clause (asc/desc)
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        asyncNewsParser = new AsyncNewsParser();
        asyncNewsParser.execute(cursor);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(NEWS_KEY, news);
        outState.putInt(LAST_PAGE_PAGINATION_KEY, lastPaginationPage);
        outState.putInt(LAST_PAGE_FRESH_NEWS, lastPageFreshNews);
        outState.putBoolean(IS_PROGRESS_BAR_SHOWING_KEY, isProgressBarShowing);
        outState.putBoolean(IS_PAGINATION_KEY, isPagination);
        outState.putBoolean(IS_LOADING_NEWS_KEY, isLoadingNews);
        outState.putBoolean(IS_REFRESHING_KEY, isRefreshing);
        outState.putLong(LAST_FRESH_NEWS_DATE_KEY, lastFreshNewsDate);
        outState.putLong(LAST_OLD_NEWS_DATE_KEY, lastOldNewsDate);
    }

    public class NewsObserver extends ContentObserver {
        private final String TAG = getClass().getSimpleName();

        public NewsObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            LogUtils.LOGD(TAG, "Content in news table is changed.");
            restartLoader(NEWS_LOADER);
        }
    }

    private class AsyncNewsParser extends AsyncTask<Cursor, Void, ArrayList<NewsItem>> {

        @Override
        protected ArrayList<NewsItem> doInBackground(Cursor... params) {
            return CursorHelper.parseNews(params[0]);
        }

        @Override
        protected void onPostExecute(ArrayList<NewsItem> parsed) {
            if (!isCancelled()) {
                if (isRefreshing) {
                    isRefreshing = false;
                    cancelRefreshing();
                }
                if (parsed.size() == 0) {
                    // App just start and we don't have cached news in the db
                    if (news == null) {
                        if (isLoadingNews) {
                            newsAdapter.showEmptyDataView();
                            isLoadingNews = false;
                        }
                        loadNewsFromNetwork();
                        return;
                    }
                    // We don't have any previous pages of news on the server
                    if (isPagination) {
                        cancelPagination();
                    } else if (isRefreshing) {
                        // We don't have any fresh pages of news on the server
                        isRefreshing = false;
                        cancelRefreshing();
                    }
                    return;
                }


                if (isLoadingNews) {
                    // If we've started to load fresh news
                    isLoadingNews = false;
                    if (news == null) {
                        lastOldNewsDate = parsed.get(parsed.size() - 1).getConvertedDate();
                        news = new ArrayList<>();
                    }
                    lastFreshNewsDate = parsed.get(0).getConvertedDate();
                    news.addAll(0, parsed);
                    newsAdapter.addNewsToStart(parsed);
                    if (isRefreshing) {
                        // If refreshing was initiated
                        // todo load next page???
                        isRefreshing = false;
                        cancelRefreshing();
                    }
                } else if (isPagination) {
                    // We've started to load old news
                    lastOldNewsDate = parsed.get(parsed.size() - 1).getConvertedDate();
                    newsAdapter.addNewsToEnd(parsed);
                    cancelPagination();
                } else {
                    // App just started and we have news cached in the db
                    lastFreshNewsDate = parsed.get(0).getConvertedDate();
                    lastOldNewsDate = parsed.get(parsed.size() - 1).getConvertedDate();
                    news = parsed;
                    newsAdapter.setNews(news);
                    loadNewsFromNetwork();
                }
            }
        }
    }

    private void hideProgressView() {
        isProgressBarShowing = false;
        newsAdapter.removeProgressView();
    }

    private void cancelPagination() {
        isPagination = false;
        newsAdapter.enablePaginationView(false);
    }
}
