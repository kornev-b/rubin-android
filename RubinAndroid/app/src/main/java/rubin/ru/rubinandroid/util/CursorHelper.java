package rubin.ru.rubinandroid.util;

import android.database.Cursor;

import java.util.ArrayList;

import rubin.ru.rubinandroid.model.NewsItem;
import rubin.ru.rubinandroid.provider.RubinContract;

/**
 * Helper class to parse model entities from the cursor.
 * Created by Bogdan Kornev
 * on 09.02.2015 at 19:52.
 */
public class CursorHelper {

    public static ArrayList<NewsItem> parseNews(Cursor cursor) {
        ArrayList<NewsItem> parsed = new ArrayList<>();

        cursor.moveToFirst();

        if (cursor.getCount() > 0) {
            do {
                NewsItem item = parseNewsItem(cursor);
                parsed.add(item);
            } while (!cursor.isClosed() && cursor.moveToNext());
        }

        return parsed;
    }

    private static NewsItem parseNewsItem(Cursor cursor) {
        NewsItem parsed = new NewsItem();
        parsed.setId(cursor.getLong(cursor.getColumnIndexOrThrow(RubinContract.NewsTable.ID)));
        parsed.setSubject(cursor.getString(cursor.getColumnIndexOrThrow(RubinContract.NewsTable.SUBJECT)));
        parsed.setBody(cursor.getString(cursor.getColumnIndexOrThrow(RubinContract.NewsTable.BODY)));
        parsed.setPublished(DateConvertUtils.convertDateInMillisToRubinDate(
                cursor.getLong(cursor.getColumnIndexOrThrow(RubinContract.NewsTable.PUBLISHED))));
        parsed.setImage(cursor.getString(cursor.getColumnIndexOrThrow(RubinContract.NewsTable.IMAGE)));
        return parsed;
    }
}
