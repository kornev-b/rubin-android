package rubin.ru.rubinandroid.network;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;
import rubin.ru.rubinandroid.model.NewsItem;
import rubin.ru.rubinandroid.model.RegisterResponse;

/**
 * Created by Bogdan Kornev
 * on 27.02.2015 at 18:15.
 */
public interface RubinRest {
    @FormUrlEncoded
    @POST("/registrations")
    public RegisterResponse register(@Field("user[email]") String email,
                                     @Field("user[login]") String login,
                                     @Field("user[password]") String password,
                                     @Field("user[password_confirmation]") String passwordConfirmation);

    @GET("/news_items")
    public NewsItem.News getNews();

    @GET("/news_items")
    public NewsItem.News getNews(@Query("page") int page);

}
