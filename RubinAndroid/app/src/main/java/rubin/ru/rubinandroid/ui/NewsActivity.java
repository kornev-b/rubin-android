package rubin.ru.rubinandroid.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;

import rubin.ru.rubinandroid.R;
import rubin.ru.rubinandroid.ui.fragment.NewsFragment;

/**
 * Created by Bogdan Kornev
 * on 27.02.2015 at 16:50.
 */
public class NewsActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(new NewsFragmentPagerAdapter());

        PagerTabStrip pagerTabStrip = (PagerTabStrip) findViewById(R.id.pager_header);
        pagerTabStrip.setTabIndicatorColorResource(R.color.theme_accent_1);
    }

    @Override
    protected int getSelfNavDrawerItem() {
        return NAVDRAWER_ITEM_NEWS;
    }

    private class NewsFragmentPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 1;

        public NewsFragmentPagerAdapter() {
            super(getSupportFragmentManager());
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = new NewsFragment();
//            Bundle bundle = new Bundle();
//            bundle.putInt(RepoType.class.getSimpleName(), position);
//            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return "All news";
            }
            return "Category";
        }
    }
}
