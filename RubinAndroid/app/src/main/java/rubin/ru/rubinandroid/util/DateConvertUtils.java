package rubin.ru.rubinandroid.util;


import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateConvertUtils {
    private static final String TAG = DateConvertUtils.class.getSimpleName();

    public static String getUpdatedDateAgo(long dateInMillis) {
        return DateUtils.getRelativeTimeSpanString(dateInMillis, (new Date()).getTime(),
                0, DateUtils.FORMAT_ABBREV_RELATIVE).toString();
    }

    public static long getDateInMillisForRubinDate(String rubinDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz", Locale.getDefault());
        Date date;

        try {
            date = sdf.parse(rubinDate);
        } catch (ParseException e) {
            LogUtils.LOGE(TAG, e.getMessage(), e.getCause());
            return 0;
        }

        return date.getTime();
    }

    public static String convertDateInMillisToRubinDate(long date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz", Locale.getDefault());
        return sdf.format(new Date(date));
    }
}
