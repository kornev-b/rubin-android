package rubin.ru.rubinandroid.network.events;

/**
 * Created by Bogdan Kornev
 * on 02.02.2015 at 13:03.
 */
public class BaseErrorEvent {
    private int responseCode;
    private String message;

    public BaseErrorEvent(int responseCode) {
        this.responseCode = responseCode;
    }

    public BaseErrorEvent(int responseCode, String message) {
        this.responseCode = responseCode;
        this.message = message;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getMessage() {
        return message;
    }
}
