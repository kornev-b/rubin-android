package rubin.ru.rubinandroid.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import rubin.ru.rubinandroid.util.LogUtils;

/**
 * Created by Bogdan Kornev
 * on 27.02.2015 at 16:39.
 */
public class RubinProvider extends ContentProvider {
    private static final int NEWS = 100;
    // The URI Matcher used by this content provider.
    private static final UriMatcher uriMatcher = buildUriMatcher();
    private static final String TAG = RubinProvider.class.getSimpleName();

    private RubinDbHelper dbHelper;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = RubinContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, RubinContract.PATH_NEWS, NEWS);
        return matcher;
    }

    @Override
    public boolean onCreate() {
        dbHelper = new RubinDbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor retCursor;

        switch (uriMatcher.match(uri)) {
            case NEWS:
                retCursor = query(projection, selection, selectionArgs,
                        sortOrder, RubinContract.NewsTable.TABLE_NAME);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        retCursor.setNotificationUri(getContext().getContentResolver(), uri);

        return retCursor;
    }

    @Override
    public String getType(Uri uri) {
        // Use the Uri Matcher to determine what kind of URI this is.
        final int match = uriMatcher.match(uri);

        switch (match) {
            case NEWS:
                return RubinContract.NewsTable.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case NEWS:
                long id = insert(db, values, RubinContract.NewsTable.TABLE_NAME,
                        SQLiteDatabase.CONFLICT_IGNORE);
                returnUri = RubinContract.NewsTable.buildReposURI(id);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return returnUri;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        switch (match) {
            case NEWS:
                return bulkInsertNotify(db, values, RubinContract.NewsTable.TABLE_NAME,
                        SQLiteDatabase.CONFLICT_IGNORE, uri);
            default:
                return super.bulkInsert(uri, values);
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        final String table;
        int rowsDeleted;

        switch (match) {
            case NEWS:
                table = RubinContract.NewsTable.TABLE_NAME;
                rowsDeleted = db.delete(
                        RubinContract.NewsTable.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        LogUtils.LOGD(TAG, rowsDeleted + " rows deleted from table " + table);

        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        final String table;
        int rowsUpdated;

        switch (match) {
            case NEWS:
                table = RubinContract.NewsTable.TABLE_NAME;
                rowsUpdated = db.update(RubinContract.NewsTable.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        LogUtils.LOGD(TAG, "updated " + rowsUpdated + " rows in table " + table);

        return rowsUpdated;
    }

    private Cursor query(String[] projection, String selection,
                         String[] selectionArgs, String sortOrder, String table) {
        return dbHelper.getReadableDatabase().query(
                table,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
    }

    private long insert(SQLiteDatabase db, ContentValues values, String table,
                        int conflictAlgorithm) {
        long id = db.insertWithOnConflict(table, null, values, conflictAlgorithm);
        if (id > 0) {
            return id;
        } else {
            return 0;
        }
    }

    private int bulkInsertNotify(SQLiteDatabase db, ContentValues[] values, String table,
                                 int conflictAlgorithm, Uri notificationUri) {
        long startTime = System.currentTimeMillis();
        LogUtils.LOGD(TAG, "beginTransaction");
        db.beginTransaction();
        int insertedCount = 0;

        try {
            for (ContentValues value : values) {
                long insertId = db.insertWithOnConflict(table,
                        null, value, conflictAlgorithm);

                if (insertId > 0) {
                    insertedCount++;
                }
            }
            db.setTransactionSuccessful();
            LogUtils.LOGD(TAG, "endTransaction: " + (System.currentTimeMillis() - startTime) + "ms");
        } finally {
            db.endTransaction();
        }

        getContext().getContentResolver().notifyChange(notificationUri, null);

        LogUtils.LOGD(TAG, "inserted " + insertedCount + " rows into table " + table);

        return insertedCount;
    }
}
