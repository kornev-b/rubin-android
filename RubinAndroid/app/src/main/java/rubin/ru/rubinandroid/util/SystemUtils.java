package rubin.ru.rubinandroid.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;

import rubin.ru.rubinandroid.BuildConfig;
import rubin.ru.rubinandroid.Config;

public class SystemUtils {

    public static int getDisplayWidth(Context context) {
        if (context == null || !(context instanceof Activity)) {
            return 0;
        }

        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        return displaymetrics.widthPixels;
    }

    public static int getDisplayHeight(Context context) {
        if (context == null || !(context instanceof Activity)) {
            return 0;
        }

        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        return displaymetrics.heightPixels;
    }

    /**
     * Overall orientation of the screen.  May be one of
     * {Configuration.ORIENTATION_LANDSCAPE}, {Configuration.ORIENTATION_PORTRAIT}.
     */
    public static int getScreenOrientation() {
        return Resources.getSystem().getConfiguration().orientation;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void copyTextToClipboard(Context context, CharSequence text) {
        if (context == null) {
            return;
        }
        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context
                .getSystemService(Context.CLIPBOARD_SERVICE);
        android.content.ClipData clip = android.content.ClipData.newPlainText("link", text);
        clipboard.setPrimaryClip(clip);
    }

    public static String getRestEndpoint() {
        return BuildConfig.DEBUG ? Config.RUBIN_ENDPOINT_DEBUG : Config.RUBIN_ENDPOINT_RELEASE;
    }

    public static void stopService(Context context, Class<?> serviceClass) {
        if (context == null) {
            return;
        }
        if (isServiceRunning(context, serviceClass)) {
            Intent serviceIntent = new Intent(context, serviceClass);
            context.stopService(serviceIntent);
        }
    }

    public static void startService(Context context, Class<?> serviceClass) {
        if (context == null) {
            return;
        }
        Intent serviceIntent = new Intent(context, serviceClass);
        context.startService(serviceIntent);
    }

    public static void startServiceIfNotStarted(Context context, Class<?> serviceClass) {
        if (context == null) {
            return;
        }
        if (!isServiceRunning(context, serviceClass)) {
            startService(context, serviceClass);
        }
    }

    public static boolean isServiceRunning(Context context, Class<?> serviceClass) {
        if (context == null) {
            return false;
        }
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
