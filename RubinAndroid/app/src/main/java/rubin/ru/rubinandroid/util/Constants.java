package rubin.ru.rubinandroid.util;

/**
 * Created by Bogdan Kornev
 * on 01.02.2015 at 15:58.
 */
public class Constants {
    public static final String ACTION_EXTRA = "action";
    public static final String USERNAME_EXTRA = "username";
    public static final String PASSWORD_EXTRA = "password";
}